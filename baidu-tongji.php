<?php
/*
 * Plugin Name: Baidu Tongji (Analytics)
 * Version: 2.0.0
 * Plugin URI: http://ficksworkshop.com/apps/162-baidu-tongji
 * Description: Adds the Baidu Tongji (Analytics) script into a WordPress page. Modified based on Haoqis's plugin (http://haoqis.com/2011/baidu-tongji-generator.html)
 * Author: Garret Fick, awesomekel
 * Author URI: http://ficksworkshop.com
 * License: BSD
 */

 /**
  * Set an option by name and store in this plugin's store.
  * @param string $option_name The option's name
  * @param any $option_value The value to associate with the option
  */
function baidu_tongji_set_option($option_name, $option_value) {
    $baidu_tongji_options = get_option('baidu_tongji_options');
    $baidu_tongji_options [$option_name] = $option_value;
    update_option('baidu_tongji_options', $baidu_tongji_options);
}

/**
 * Gets an option value by name.the value of the specified option.
 *
 * If the option doesn't exist, this will create a default value for all options.
 * This means, if you create a new option, it will cause a reset of all values.
 *
 * @param string $option_name The option's name
 * @return any The stored value. The type depends on the option being retrieved
 */
function baidu_tongji_get_option($option_name) {

    $baidu_tongji_options = get_option('baidu_tongji_options');

    if (!$baidu_tongji_options || !array_key_exists($option_name, $baidu_tongji_options)) {

        $baidu_tongji_default_options = array();
        $baidu_tongji_default_options ['site_code'] = "";
        $baidu_tongji_default_options ['enable_tracker'] = true;
        $baidu_tongji_default_options ['track_adm_pages'] = false;
        $baidu_tongji_default_options ['track_logged_in_users'] = false;
        $baidu_tongji_default_options ['allow_cookie_disable'] = false;

        add_option('baidu_tongji_options', $baidu_tongji_default_options, __('SettingsPage', 'btj'));

        $result = $baidu_tongji_default_options [$option_name];
    } else {

        $result = $baidu_tongji_options [$option_name];
    }
    
    return $result;
}

/**
 * Do we allow disable by cookie and are we actually disabled?
 * @return boolean True if the cookie exists and it is set to disable
 */
function baidu_tongji_is_cookie_disabled()
{
    return baidu_tongji_get_option('allow_cookie_disable') ? ($_COOKIE["baidutongjidisabled"] == "true") : false;
}

/**
 * Output a tip-formatted message
 * @param string msg The message to display
 */
function baidu_tongji_tip($msg) {

    return '<div class="updated"><p><strong>' . $msg . '</strong></p></div>';
}

/**
 * Output an error-formatted message
 * @param string msg The message to display
 */
function baidu_tongji_error($msg) {

    return '<div class="error settings-error"><p><strong>' . $msg . '</strong></p></div>';
}

/**
 * Registered function to generate the options page
 * @param array $options Current options array
 */
function baidu_tongji_admin_html($options) {
    $enable_tracker = $options['enable_tracker'] ? ' checked="true"' : '';
    $track_adm_pages = $options['track_adm_pages'] ? ' checked="true"' : '';
    $track_logged_in_users = $options['track_logged_in_users'] ? ' checked="true"' : '';
    $allow_cookie_disable = $options['allow_cookie_disable'] ? ' checked="true"' : '';
?>
<div class=wrap>
    <form method="post">
        <h2><?php _e('Settings', 'btj'); ?></h2>
        <fieldset class="options" name="general">
            <legend><?php _e('PasteCode', 'btj'); ?></legend>
            <textarea rows="5" class="large-text code" id="site_code" name="site_code"><?php echo esc_html($options['site_code']); ?></textarea>
            
            <table class="form-table"><tbody>
            <tr>
            <td><?php _e('Enable', 'btj'); ?></td>
            <td>
            <label for="enable_tracker">
            <input type="checkbox" value="true" id="enable_tracker" name="enable_tracker" <?php echo $enable_tracker ?>/>
            <?php _e('EnableDetail', 'btj'); ?></label>
            </td>
            </tr>
            
            <tr>
            <td><?php _e('TrackAdminDetail', 'btj'); ?></td>
            <td>
            <label for="track_adm_pages">
            <input type="checkbox" value="true" id="track_adm_pages" name="track_adm_pages" <?php echo $track_adm_pages ?>/>
            <?php _e('TrackAdmin', 'btj'); ?></label>
            </td>
            </tr>
            
            <tr>
            <td><?php _e('TrackLoggedIn', 'btj'); ?></td>
            <td>
            <label for="track_logged_in_users">
            <input type="checkbox" value="true" id="track_logged_in_users" name="track_logged_in_users" <?php echo $track_logged_in_users ?>/>
            <?php _e('TrackLoggedInDetail', 'btj'); ?></label>
            </td>
            </tr>
            
            <tr>
            <td><?php _e('AllowCookieDisable', 'btj'); ?></td>
            <td>
            <label for="allow_cookie_disable">
            <input type="checkbox" value="true" id="allow_cookie_disable" name="allow_cookie_disable" <?php echo $allow_cookie_disable ?>/>
            <?php _e('AllowCookieDisableDetail', 'btj'); ?></label>
            <p class="description"><?php _e('AllowCookieDisableDesc', 'btj'); ?></p>
            </td>
            </tr>
            
            </tbody></table>
            
            <p class="submit"><input type="submit" value="<?php _e('SaveSettings', 'btj'); ?>" class="button-primary" name="submit"></p>
        </fieldset>
    </form>
</div>
<?php
}

/**
 * Registered function when the options page is saved
 */
function baidu_tongji_options() {

    $code = trim($_POST ['site_code']);
    $submit = trim($_POST ['submit']);
    
    if ($submit) {
        if ($code) {
            baidu_tongji_set_option('site_code', $code);
            echo baidu_tongji_tip(__('SettingsSaved','btj'));
        } else {
            echo baidu_tongji_error(__('SiteCodeNotSaved','btj'));
        }

        if (isset($_POST ['track_adm_pages'])) {
            baidu_tongji_set_option('track_adm_pages', true);
        } else {
            baidu_tongji_set_option('track_adm_pages', false);
        }
        if (isset($_POST ['enable_tracker'])) {
            baidu_tongji_set_option('enable_tracker', true);
        } else {
            baidu_tongji_set_option('enable_tracker', false);
        }
        if (isset($_POST ['track_logged_in_users'])) {
            baidu_tongji_set_option('track_logged_in_users', true);
        } else {
            baidu_tongji_set_option('track_logged_in_users', false);
        }
        if (isset($_POST ['allow_cookie_disable'])) {
            baidu_tongji_set_option('allow_cookie_disable', true);
        } else {
            baidu_tongji_set_option('allow_cookie_disable', false);
        }
    }

    baidu_tongji_admin_html(get_option('baidu_tongji_options'));

    if (!baidu_tongji_is_cookie_disabled() and baidu_tongji_get_option('track_adm_pages')) {
        add_action('admin_footer', 'baidu_tongji_footer');
    } else {
        remove_action('admin_footer', 'baidu_tongji_footer');
    }
}

/**
 * Registered function to create the options page
 */
function baidu_tongji_admin() {
    if (function_exists('add_options_page')) {
        add_options_page(__('BaiduAnalyticsPage', 'btj'), __('BaiduAnalytics', 'btj'), 'manage_options', 'baidu_tongji', 'baidu_tongji_options');
    }
}

/**
 * Registered function for add to the footer (the tracker code)
 */
function baidu_tongji_footer()
{
    if (baidu_tongji_get_option('enable_tracker') && (!is_user_logged_in() || baidu_tongji_get_option('track_logged_in_users'))) {
        $html = stripslashes(baidu_tongji_get_option('site_code'));
        echo $html;
    }
}

/**
 * Registered function to load the language files
 */
function baidu_tongji_load_textdomain()
{
	load_plugin_textdomain('btj', false, dirname(plugin_basename(__FILE__)) . '/lang/');
}

/**
 * Registered function to convert the short code into the appropriate HTML
 * We put the javascript inline because real sites would only use this on an internally-known
 * page. We don't want to load this every time.
 */
function baidu_tongji_shortcode()
{
    $output = '<a href="#" onclick="baidu_tongji_alert()">' . __('CheckStatus', 'btj') . '</a><br/><a href="#" onclick="baidu_tongji_toggle()">' . __('ToggleStatus', 'btj') . '</a>';
    $output .= '<script type="text/javascript">function baidu_tongji_alert()
    {
        alert(baidu_tongji_has_disable_cookie() ? "' . __('CookieSet', 'btj') . '" : "' . __('CookieNotSet', 'btj') . '")
    }
    function baidu_tongji_toggle()
    {
        document.cookie = (baidu_tongji_has_disable_cookie()) ? "baidutongjidisabled=false" : "baidutongjidisabled=true";
    }
    function baidu_tongji_has_disable_cookie()
    {
        return (document.cookie.replace(/(?:(?:^|.*;\s*)baidutongjidisabled\s*\=\s*([^;]*).*$)|^.*$/, "$1") == "true")
    }
    </script>';
    return $output;
}

add_action('admin_menu', 'baidu_tongji_admin');

if (!baidu_tongji_is_cookie_disabled() and baidu_tongji_get_option('enable_tracker'))
{
        add_action('wp_footer', 'baidu_tongji_footer');
}

add_action('plugins_loaded', 'baidu_tongji_load_textdomain');

add_shortcode('baidu-tongji-switch', 'baidu_tongji_shortcode');

?>