��          �      |      �     �               4     C     V     b  	   o     y     �  	   �     �     �     �     �     �     �  
   �     �            �  $     �  �   �  9   �     �     �       2   #  <   V     �     �     �     �     �     �       4        I     _  <   q     �  8   �              
                      	                                                             AllowCookieDisable AllowCookieDisableDesc AllowCookieDisableDetail BaiduAnalytics BaiduAnalyticsPage CheckStatus CookieNotSet CookieSet Enable EnableDetail PasteCode SaveSettings Settings SettingsPage SettingsSaved SiteCodeNotSaved ToggleStatus TrackAdmin TrackAdminDetail TrackLoggedIn TrackLoggedInDetail Project-Id-Version: BaiduAnalytics
POT-Creation-Date: 2014-11-28 14:46+0800
PO-Revision-Date: 2014-11-28 14:48+0800
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.10
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: ..
 Allow Disable via Cookie When enabled, you can disable analytics for particular computers by setting an appropriate cookie. If the cookie exists, the computer is not tracked. You can quickly enable/disable via the [baidu-tongji-switch] shortcode. When enabled, allow setting a cookie to disable tracking. Baidu Analytics Baidu Analytics Settings Check Disable Cookie Analytics collected from visits from this browser. Analytics are not be collected for visits from this browser. Enable Enable analytics Paste Tracker Code Below Save Settings Baidu Analytics Settings Baidu Analytics Settings Settings saved. Site code not saved because the format is incorrect. Toggle Disable Cookie Track Admin Pages When enabled, use the tracking code for administrative pages Track Logged In Users When enabled, track all users, including logged in users 