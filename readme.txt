﻿=== Baidu Tongji ===
Plugin Name: Baidu tongji
Contributors: 
Donate link: http://ficksworkshop.com/apps/162-baidu-tongji
Tags: baidu, tongji, analytics, statistics, stats, tracking
Requires at least: 4.0
Tested up to: 4.0
Stable tag: trunk

Add Baidu Tongji (百度统计) JavaScript to each page on your WordPress.

== Description ==
Add Baidu Tongji (百度统计) JavaScript to each page on your WordPress website with several options to control when the code is inserted to the page.

This is based on the Baidu Tongji Generator by haoqisir and webbeast (http://haoqis.com/2011/baidu-tongji-generator.html).

== Installation ==
1. Upload  to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. admin -> "Settings" -> 

== Frequently Asked Questions ==

= Where can I find more information? =

Visit the [official webpage on Fick's Workshop](http://ficksworkshop.com/apps/162-baidu-tongji) for installation instructions, usage documentation and official releases.

= What is the difference between this and Baidu Tongji Generator =

1. Additional options to control when to output the tracker code
2. The WordPress administrator interface is English (and can be translated)

= Where can I find the original Baidu Tongji Generator =

You can find the original 

= Why was this forked? =

Nothing sinister here - simply a short project time schedule. I needed to have the code implemented as fast as possible, and forking was faster than asking the original authors to make modifications.

= Can I use this for Google Analytics =

The code doesn't actually know anything specific about Baidu Analytics. It can be used to insert JavaScript into any page.

== Changelog ==
= 2.0.0 =
 * Forked from Baidu Tongji Generator
 * Added new options to control when the code is inserted